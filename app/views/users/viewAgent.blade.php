<h2 class="post-listings">Agent <strong>{{$agent->username}}</strong>'s rates </h2>
<hr>
<div class="clear-fix"></div>
@if($agentRates->count())
<table>
    <thead>
    <tr>
        <th width="130">Customer</th>
        <th width="180">Rate score</th>
        <th width="240" class="text-center">Rate Time</th>
        <th width="110">Comment</th>
    </tr>
    </thead>
    <tbody>
    @foreach($agentRates as $rate)
    <tr>
        <td>{{ $rate->rated_by }}</td>
        <td>{{ $rate->point }}</td>
        <td class="text-center">{{ date('m/d/Y H:i',$rate->rated_time)}}</td>
        <td>
            @if($rate->comment) <a data-email="{{$rate->rated_by}}" title="{{{$rate->comment}}}" class="viewComment" href="javascript:">View</a> @endif
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@else
<p>Sorry, {{$agent->username}} has no rate</p>
@endif
{{$agentRates->appends(\Input::all())->links()}}
<script>
$(function(){
    $('a.viewComment').on('click', function() {
        var template =  "<p><b>Commenter: </b>"+$(this).data('email')+"</p>"+
                        "<p><b>Comment:</b></p>"+
                        "<p>"+$(this).attr("title")+"</p>"+
                        '<a class="close-reveal-modal">&#215;</a>';
        $('#comment-show').html(template).foundation('reveal', 'open');
    });
})
</script>

