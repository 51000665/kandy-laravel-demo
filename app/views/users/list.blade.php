{{ HTML::style('assets/jquery-ui/jquery-ui.css') }}
{{ HTML::script('assets/jquery-ui/jquery-ui.js') }}

{{ HTML::style('assets/select2-3.5.2/select2.css') }}
{{ HTML::script('assets/select2-3.5.2/select2.js') }}

{{ HTML::style('assets/editable/css/jqueryui-editable.css') }}
{{ HTML::script('assets/editable/js/jqueryui-editable.js') }}

<h2 class="post-listings">User listings</h2>
<hr>
<div>
    <button type="button" class="tiny radius sync-user" style="float: right;margin-right: 50px;">Sync Kandy Users
    </button>
    <button type="button" class="tiny radius assign-user" style="float: right;margin-right: 20px;"
            data-href="{{ URL::action('UserController@assignkandyuser') }}">Assign Kandy Users
    </button>
</div>
<div class="clear-fix"></div>
<table>
    <thead>
    <tr>
        <th width="130">Username</th>
        <th width="180">Email</th>
        <th width="240" class="text-center">Kandy Account</th>
        <th width="110"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
    <tr>
        <td>{{ $user->username }}</td>
        <td>{{ $user->email }}</td>
        <td class="text-center">{{ $user->getKandyUser() }}</td>
        <td>{{ HTML::linkRoute('user.delete','Delete',$user->id) }}</td>
    </tr>
    @endforeach
    </tbody>
</table>
{{$users->links()}}

<script>
    $.fn.editable.defaults.mode = 'inline';
    var editableInit = function () {
        $('.kandy-editable').editable({
            source: '{{ URL::action("UserController@listKandyuser") }}',
            select2: {
                placeholder: 'Select User',
                minimumInputLength: 0
            },
            success: function (response, newValue) {
                $('.kandy-editable').each(function (index) {
                    var me = $(this);
                    processDisplayButtons(me);
                });
            }
        });
        $('.kandy-editable').each(function (index) {
            var me = $(this);
            processDisplayButtons(me);
        });
    }

    var processDisplayButtons = function (link) {
        link.siblings('button').show();
        if (link.hasClass('editable-empty')) {
            link.siblings(".custom-cancel-button").hide();
        } else {
            link.siblings(".custom-add-button").hide();
        }
    }

    $(document).ready(function () {
        editableInit();

        $('.kandy-editable').on('shown', function (ev, edittable) {
            $(this).parent().find(".custom-button").hide();
        })

        $('.kandy-editable').on('hidden', function (e, reason) {
            var me = $(this);
            processDisplayButtons(me);
        })

        $('.custom-cancel-button').on('click', function (e) {
            e.preventDefault();
            var me = $(this);
            var main_user_id = me.attr("data-id");

            $.ajax({
                type: "POST",
                url: '{{ URL::action("UserController@unassignkandyuser") }}',
                data: {pk: main_user_id}
            }).done(function () {
                    var link = me.siblings("a");
                    // reset html
                    link.html("not selected");
                    link.addClass("editable-empty");
                    editableInit();
                }).fail(function () {
                    alert("Sorry! There was an error with your request.")
                });
        })

        $('.custom-add-button').on('click', function (e) {
            e.preventDefault();
            var me = $(this);
            var main_user_id = me.attr("data-id");

            $.ajax({
                type: "POST",
                url: '{{ URL::action("UserController@updatekandyuser") }}',
                data: {pk: main_user_id}
            }).done(function (data) {
                    var link = me.siblings("a");
                    // reset html
                    link.html(data.user);
                    link.removeClass("editable-empty");
                    editableInit();
                }).fail(function (e) {
                    alert("Sorry! There was an error with your request. " + e.responseText);
                });
        })

        $(".sync-user").click(function (e) {
            if (!confirm("Are you sure that you want to sync users from Kandy server with your local data?")) {
                return;
            }
            var me = $(this);
            me.attr('disabled', true);
            $.ajax({
                url: "{{ URL::action('UserController@synckandyuser') }}",
                type: "GET"
            }).done(function (data) {
                    alert("Synchronization successfully!");
                }).fail(function (error) {
                    alert("Sorry! There was an error with your request. " + error);
                }).always(function () {
                    me.attr('disabled', false);
                });
        })
        $(".assign-user").click(function () {
            var url = $(this).attr('data-href');
            window.location.href = url;
        });
    });
</script>
<style>
    .text-center {
        text-align: center;
    }

    .select2-results {
        font-size: 0.8em;
    }

    .select2-choice {
        min-width: 180px !important;
    }

    .editable-cancel {
        display: none;
    }

    .clear-fix {
        clear: both;
    }
</style>