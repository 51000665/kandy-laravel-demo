{{ HTML::style('assets/select2-3.5.2/select2.css') }}
{{ HTML::script('assets/select2-3.5.2/select2.js') }}
<h2 class="post-listings">Chat Agent listings</h2>
<hr>
<div style="width: 40%; margin-left: 56%;">
<input class="select2" id="userSearch">
<button id="btnAddUser" class="tiny">Add</button>
</div>
<div class="clear-fix"></div>
<table>
    <thead>
    <tr>
        <th width="130">Username</th>
        <th width="180">Email</th>
        <th width="240" class="text-center">Kandy Account</th>
        <th>Average score</th>
        <th width="110"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
    <tr>
        <td>{{ $user->username }}</td>
        <td>{{ $user->email }}</td>
        <td class="text-center">{{ $user->kandy_user}}</td>
        <td>{{round($user->average,1)}}</td>
        <td>
        {{ HTML::linkRoute('user.removechatagent','Delete',array('id' => $user->id)) }} /
        {{HTML::linkRoute('user.viewagent', 'View', array('agentId' => $user->main_user_id))}}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
{{$users->links()}}

<script>
    $(function(){
        $("#userSearch").select2({
            width: '200px',
            ajax: {
                 quietMillis: 100,
                 url: "/kandy/getUserForAgent",
                 dataType: 'json',
                 delay: 250,
                 data: function (params) {
                     return {
                         q: params
                     };
                 },
                 results: function (data) {
                     return {results: data.results};
                 }
                },
                minimumInputLength: 1
         });

         $("#btnAddUser").click(function(e){
            var data = $('#userSearch').select2('data') || {};
            $.ajax({
                url: '/admin/user/addchatagent',
                data: {id:data.id},
                type: 'GET',
                success: function(){
                    window.location.reload();
                }
            })
         })

    })
</script>