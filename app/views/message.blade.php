<div class="message {{ $class }}">
    @if( isset($heading) )
    <h1>{{ $heading }}</h1>
    @else
    <h1>Error!</h1>
    @endif
    @foreach ($messages as $message)
    <p>{{ nl2br( $message ) }}</p>
    @endforeach
</div>
