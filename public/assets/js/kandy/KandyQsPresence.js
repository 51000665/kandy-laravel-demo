/**
 * Callback when login successfully
 */
window.login_success_callback = function () {
    changeUIState("LOGGED_IN");
}

/**
 * Callback when login fail
 */
window.login_failed_callback = function () {
    $("#loading h2").html("Failed to load Kandy components. Please contact admin for detail.")
}

/**
 * Change user state
 * @param state
 */
window.changeUIState = function (state) {
    switch (state) {
        case 'LOGGED_OUT':
            $("#loginForm").show();
            $("#topRow").hide();
            $('#contactsAndDirSearch').hide();
            $("#dirSearchResults div:not(:first)").remove();
            $("#myContacts div:not(:first)").remove();
            break;
        case 'LOGGED_IN':
            $("#loginForm").hide();
            $("#topRow").show();
            $("#directorySearch").show();
            $('#contactsAndDirSearch').show();
            break;
    }
}
