/**
 * Callback when login successfully.
 */
window.login_success_callback = function () {
    changeUIState('READY_FOR_CALLING');
}

/**
 * Callback when login fail
 */
window.login_failed_callback = function () {
    //$("#loading h2").html("Failed to load Kandy components. Please contact admin for detail.")
}

/**
 * Callback when incoming call.
 * @param call
 * @param isAnonymous
 */
window.incoming_call_callback = function (call, isAnonymous) {
    changeUIState('BEING_CALLED');
}

/**
 * Callback when on call.
 * @param call
 */
window.on_call_callback = function (call) {
    changeUIState("ON_CALL");
}

/**
 * Callback when answer call.
 * @param call
 * @param isAnonymous
 */
window.call_answered_callback = function (call, isAnonymous) {
    changeUIState("ON_CALL");
}

/**
 * Callback when end call.
 */
window.call_ended_callback = function () {
    changeUIState('READY_FOR_CALLING');
}

/**
 * Callback when click AnswerVideo Button
 * @param stage
 */
window.answer_video_call_callback = function (stage) {
    changeUIState(stage);
}

/**
 * Callback when click AnswerVideo Button
 * @param stage
 */
window.answer_voice_call_callback = function (stage) {
    changeUIState(stage);
}

/**
 * Callback when click Call Button
 * @param stage
 */
window.makeCall_callback = function (stage) {
    changeUIState(stage);
}

/**
 * Callback when click End call Button
 * @param stage
 */
window.end_call_callback = function (stage) {
    changeUIState(stage);
}

/**
 * Change user state.
 *
 * @param state
 */
window.changeUIState = function (state) {
    switch (state) {
        case 'LOGGED_OUT':
            $('#loggedIn').hide();
            break;
        case 'READY_FOR_CALLING':
            $('#loggedIn').show();
            $("#loading").hide();
            $("#voiceCallWrapper").show();
            break;
        case 'BEING_CALLED':
            $('#loggedIn').hide();
            break;
        case 'CALLING':
            $('#loggedIn').hide();
            break;
        case 'ON_CALL':
            $('#loggedIn').hide();
            break;
    }
}
