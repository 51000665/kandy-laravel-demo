<?php

class PhoneController extends BaseController
{
    /**
     * Video Call Demo
     */
    public function videoAnswer()
    {
        $userId = Auth::user()->id;
        $data = array(
            "userId" => $userId
        );

        $this->layout->title = 'Kandy Video Test';
        $this->layout->main = View::make('kandy')->nest('content','phone.KandyQsVideoAnswer',$data);
    }

    /**
     * Voice Call Demo
     */
    public function voiceCall()
    {
        $userId = Auth::user()->id;
        $data = array(
            "userId" => $userId
        );

        $this->layout->title = 'Kandy Voice Call Test';
        $this->layout->main = View::make('kandy')->nest('content','phone.KandyQsVoice',$data);
    }

    /**
     * Presence List Demo
     */
    public function presenceList()
    {
        $userId = Auth::user()->id;
        $data = array(
            "userId" => $userId
        );

        $this->layout->title = 'Kandy Voice Call Test';
        $this->layout->main = View::make('kandy')->nest('content','phone.KandyQsPresence',$data);
    }

    /**
     * Chat Demo
     */
    public function chat()
    {
        $userId = Auth::user()->id;
        $kandyUser = KandyLaravel::getUser($userId);
        $data = array(
            "userId" => $userId,
            "kandyUser" => $kandyUser
        );

        $this->layout->title = 'Kandy Chat Test';
        $this->layout->main = View::make('kandy')->nest('content','phone.KandyQsChat',$data);
    }
}
