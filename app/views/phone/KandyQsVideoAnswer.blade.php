<?php // Initialize Kandy setting ?>
{{KandyLaravel::init($userId);}}
{{ HTML::style('assets/css/KandyQsVideoAnswer.css') }}
{{ HTML::script('/assets/js/kandy/KandyQsVideoAnswer.js') }}
<div>
    <h2>Make a Video Call</h2>
    <h4>This sample application demonstrates the code for answering a video call with Kandy.</h4>

    <div id="loading"><h2>Loading Kandy Components ...</h2></div>

    <div id="btnAnswerVideoArea" style="display: none">
        {{KandyButton::videoCall(array(
            "id" => "kandyVideoAnswerButton",
            "class" => "myButtonStyle",
            "options" => array(
                "callOut"      => array(
                    "id"       => "callOut",
                    "label"    => "User to call",
                    "btnLabel" => "Call"
                ),
                "calling"      => array(
                    "id"       => "calling",
                    "label"    => "Calling...",
                    "btnLabel" => "End Call"
                ),
                "incomingCall" => array(
                    "id"       => "incomingCall",
                    "label"    => "Incoming Call",
                    "btnLabel" => "Answer"
                ),
                "onCall"       => array(
                    "id"       => "onCall",
                    "label"    => "You're connected!",
                    "btnLabel" => "End Call"
                ),
            )
        ))
        }}
    </div>
    <div id="videos" style="display: none">
        {{KandyVideo::show(
            array(
                "title" => "Them",
                "id" => "theirVideo",
                "class" => "myVideoStyle",
                "htmlOptions" =>
                    array(
                        "style" => "width: 340px;
                                    height: 250px;
                                    background-color: darkslategray"
                    )
            )
        )}}

        {{KandyVideo::show(
             array(
                "title" => "Me",
                "id" => "myVideo",
                "class" => "myStyle",
                "htmlOptions" =>
                    array(
                    "style" => "width: 340px;
                                height: 250px;
                                background-color: darkslategray"
                    )

             )

        )}}

    </div><!--endD video -->

</div>
