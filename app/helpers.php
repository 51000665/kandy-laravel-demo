<?php

/**
 * get id of role user
 *
 * @return mixed
 */
function getRoleUserId()
{
    $result = \Toddish\Verify\Models\Role::firstOrCreate(array('name' => 'User', 'level' => 5))->id;
    return $result;
}

/**
 * get id of role admin
 *
 * @return mixed
 */
function getRoleAdminId()
{
    $result = \Toddish\Verify\Models\Role::firstOrCreate(array('name' => 'Super Admin', 'level' => 10))->id;
    return $result;
}