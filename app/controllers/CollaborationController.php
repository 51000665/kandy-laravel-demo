<?php
/**
 * Created by PhpStorm.
 * User: Khanh
 * Date: 3/6/2015
 * Time: 3:14 PM
 */

class CollaborationController extends BaseController {

    public function coBrowsing()
    {
        $userId = Auth::user()->id;
        $data = array(
            "userId" => $userId
        );
        $this->layout->title = 'Kandy CoBrowsing Demo';
        $this->layout->main = View::make('kandy')->nest('content','collaboration.cobrowsing', $data);
    }

} 